/* 
 * File:   main.cpp
 * Author: Jon C. Hammer
 *
 * Created on August 25, 2016, 12:04 PM
 */

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include "Matrix.h"
using namespace std;

void parseArguments(int argc, char** argv, string& inputFilename, 
    string& outputFilename, int& xIndex, int& yIndex, int& zIndex)
{
    inputFilename  = argv[1];
    outputFilename = "";
    
    xIndex = -1;
    yIndex = -1;
    zIndex = -1;
    
    for (int i = 2; i < argc; ++i)
    {
        if (strcmp(argv[i], "-o") == 0)
        {
            outputFilename = argv[i + 1];
            ++i;
        }
        else if (strcmp(argv[i], "-x") == 0)
        {
            xIndex = atoi(argv[i + 1]);
            ++i;
        }
        else if (strcmp(argv[i], "-y") == 0)
        {
            yIndex = atoi(argv[i + 1]);
            ++i;
        }
        else if (strcmp(argv[i], "-z") == 0)
        {
            zIndex = atoi(argv[i + 1]);
            ++i;
        }
    }
    
    // Assign default values if they haven't been provided
    if (outputFilename == "")
        outputFilename = "output.html";
    if (xIndex == -1)
        xIndex = 0;
    if (yIndex == -1)
        yIndex = 1;
    if (zIndex == -1)
        zIndex = 2;
}

void writeHeader(ofstream& dout, const string& title)
{
    dout << "<html>" << endl;
    dout << "<head>" << endl;
    dout << "<title>" << title << "</title>" << endl;
    dout << "<meta http-equiv=\"content-type\" content=\"text/html; charset=ISO-8859-1\">" << endl;
    dout << "<script type=\"text/javascript\">" << endl;
    dout << "var points = [" << endl;
}

bool writeOutput(const string& filename, Matrix& m, int xIndex, int yIndex, int zIndex)
{
    ofstream dout(filename.c_str());
    if (!dout) return false;
    
    // Write the header
    writeHeader(dout, filename);
    
    // Write the data
    for (size_t i = 0; i < m.rows(); ++i)
    {
        dout << m[i][xIndex] << ",";
        dout << m[i][yIndex] << ",";
        dout << m[i][zIndex] << ",";
        dout << endl;
    }
    
    // Write the rest of the file
    ifstream din("./data/tail.txt");
    if (!din) return false;
    
    string line;
    while (getline(din, line))
        dout << line << endl;
    
    din.close();
    dout.close();
    return true;
}

// arffplotter input.arff -o output.html -x 4 -y 7 -z 1
int main(int argc, char** argv)
{
    if (argc <= 1)
    {
        cerr << "No input file provided! Example usage:" << endl;
        cerr << "   arffplotter input.arff" << endl;
        return 1;
    }
    
    string inputFilename;
    string outputFilename;
    int xIndex, yIndex, zIndex;
    parseArguments(argc, argv, inputFilename, outputFilename, 
        xIndex, yIndex, zIndex);
    
    Matrix matrix;
    matrix.loadARFF(inputFilename);
    
    if (writeOutput(outputFilename, matrix, xIndex, yIndex, zIndex))
        cout << outputFilename << " successfully created." << endl;

    else cerr << "Unable to create file: " << outputFilename << endl;
    
    return 0;
}

